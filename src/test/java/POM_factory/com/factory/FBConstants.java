package POM_factory.com.factory;

final class FBConstants {

	public static final String PASS = "PASS";
	public static final String Email = "//*[@id='email']";
	public static final String PASSWORD = "//*[@id='pass']";
	public static final String LOGIN = "//*[@id='loginbutton']";
	public static final String URL = "https://www.facebook.com/";
	public static final String PROFILE = "//*[@id='pagelet_welcome_box']/ul/li/div/a";
	public static final String USERNAME = "jyosh.maddy@gmail.com";
	public static final String EPASSWORD = "1Sanjay";
	public static final String PASSWORDSETTINGSEDIT ="(//span[@class='uiIconText fbSettingsListItemEdit'])[4]";
	public static final String CURRENTPASSWORD = "//*[@id='password_old']";
	public static final String RADIOBUTTON = "//input[@value='kill_sessions']";

}
