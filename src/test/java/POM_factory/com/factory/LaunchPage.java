package POM_factory.com.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Log;

import POM.Base.BasePage;

public class LaunchPage extends BasePage{
	//WebDriver driver;
	public LaunchPage(WebDriver driver, ExtentTest test){
		super(driver,test);
	}
	
	public LoginPage goToLoginPage(){
		test.log(LogStatus.INFO,"opening with URL--"+FBConstants.URL);
		driver.get(FBConstants.URL);
		//PageFactory.initElements(driver,LoginPage.class);
		//now to add extent reports I have to write two lines
		LoginPage logp=new LoginPage(driver, test);
		 PageFactory.initElements(driver,logp);
		 return logp;
	}

}
