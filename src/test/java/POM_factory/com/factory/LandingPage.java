package POM_factory.com.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.ExtentTest;

import POM.Base.BasePage;

public class LandingPage extends BasePage {
	WebDriver driver;
	@FindBy(xpath=FBConstants.PROFILE)
	public WebElement profile;
	public LandingPage(WebDriver driver,ExtentTest test){
		super(driver,test);
	}
	
	public ProfilePage gotoProfile(){
		profile.click();
		ProfilePage pp=new ProfilePage(driver, test);
		 PageFactory.initElements(driver,pp);
		 return pp;
		
	}

}
