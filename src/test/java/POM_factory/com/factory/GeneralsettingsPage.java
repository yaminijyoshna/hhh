package POM_factory.com.factory;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;





import POM.Base.BasePage;

public class GeneralsettingsPage extends BasePage {
	
	@FindBy(xpath="//*[@id='SettingsPage_Content']/ul/li[4]/a/span[1]")
	public WebElement passwordSettings;
	@FindBy(xpath=FBConstants.CURRENTPASSWORD)
	public WebElement currentpassword;
	@FindBy(xpath="//*[@id='password_new']")
	public WebElement newpassword;
	@FindBy(xpath="//*[@id='password_confirm']")
	public WebElement Retypepassword;
	@FindBy(xpath="//input[@value='Save Changes']")
	public WebElement Savechanges;
	@FindBy(xpath="//button[text()='Continue']")
	public WebElement continueButton;
	@FindBy(xpath=FBConstants.RADIOBUTTON)
	public WebElement radioutton;
	
	public GeneralsettingsPage(WebDriver driver,ExtentTest test){
		super(driver,test);
		
		
	}
   public void goTOPasswordChange(){
		test.log(LogStatus.INFO,"do password change");
		if(!isElementPresent("//*[@id='SettingsPage_Content']/ul/li[4]/a/span[1]")){
			test.log(LogStatus.FAIL,"clciking on passwordSettings on general settinfs page failed");
			takeScreenShot();
			Assert.fail("clciking on passwordSettings on general settinfs page failed");//or reportfailure function
			}
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		passwordSettings.click();
    }
	public String doPasswordChange(String Currentpass, String newPass,String ConfirmPassword){
		
		//passwordSettings.click();
		currentpassword.sendKeys(Currentpass);
		newpassword.sendKeys(newPass);
		Retypepassword.sendKeys(ConfirmPassword);
		Savechanges.click();
		if(!isElementPresent(FBConstants.RADIOBUTTON))
			return "unsucessful";
			radioutton.click();
			continueButton.click();
			return "success";
	}
}

