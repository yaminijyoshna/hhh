package POM_factory.com.factory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import POM.Base.BasePage;

public class LoginPage  extends BasePage{
	//WebDriver driver;
	@FindBy(xpath=FBConstants.Email)
	public WebElement email;
	
	@FindBy(xpath=FBConstants.PASSWORD)
	public WebElement password;
	
	@FindBy(xpath=FBConstants.LOGIN)
	public WebElement login;
	
	public LoginPage(WebDriver driver,ExtentTest test){
		super(driver,test);
	}
	
	public Object doLogin(String UserName,String Password){
		email.sendKeys(UserName);
		password.sendKeys(Password);
		login.sendKeys(Keys.ENTER);
		boolean loginSuccess=true;
		if(loginSuccess){
		//return PageFactory.initElements(driver,LandingPage.class);
			//now to add extent reports I have to write two lines
			test.log(LogStatus.INFO,"navigating to landing page");
		LandingPage landPage=new LandingPage(driver,test);
		 PageFactory.initElements(driver,landPage);//to init @FindBy elements
		 return landPage;
		}
		else{
			test.log(LogStatus.INFO,"navigating  back to login page");
			LoginPage logp=new LoginPage(driver, test);
		  PageFactory.initElements(driver, logp);
		  return logp;
	}
		

}
}
