package POM.Base;


import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;




import org.openqa.selenium.support.PageFactory;

import POM_factory.com.factory.GeneralsettingsPage;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TopMenu  {//encapsulate commomfeatures of the pages
	WebDriver driver;
	@FindBy(xpath="//span[text()='Settings']")
	 public WebElement settings;
	
	@FindBy(xpath="//*[@id='userNavigationLabel']")
	public WebElement navigation;
	
	@FindBy(xpath="//*[@id='BLUE_BAR_ID_DO_NOT_USE']/div/div/div[1]/div/div/ul/li[12]/a/span/span")
	public WebElement logout;
	ExtentTest test;
	public TopMenu(WebDriver driver,ExtentTest test){
		this.driver=driver;
		this.test=test;
		}
	
	
	public void logout(){
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		navigation.click();
		logout.click();
		
	}
	public void goTolandingPage(){
		
	}
	public GeneralsettingsPage goToSettings(){
		test.log(LogStatus.INFO, "clicking the settings");
		navigation.click();
		//JavascriptExecutor js=(JavascriptExecutor)driver;
		//js.executeScript("document.getElementById('navigation').click()");
		settings.click();
		test.log(LogStatus.INFO, "clicked on settings");
		GeneralsettingsPage settings=new GeneralsettingsPage(driver, test);
		PageFactory.initElements(driver,settings);
		return settings;
		
	}
	
	
	

}
