package POM.Base;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BasePage {
	public WebDriver driver;
	 public TopMenu menu;
	
	public ExtentTest test;
	public BasePage(WebDriver driver, ExtentTest test){
		this.driver=driver;
		this.test=test;
		//menu=new TopMenu(driver);
	    //menu=PageFactory.initElements(driver, TopMenu.class);
		menu=new TopMenu(driver,test);
		PageFactory.initElements(driver, menu);
		
		}
	
	
	
	
	public String reportPass(String Message){
		return "PASS";
		}
    public TopMenu getMenu(){//I can do simpy BasePage.menu but calling like Basepage.getMenu profesional way
    	//of calling using getters and setters
		return menu;
    	
    }
    
    public String takeScreenShot(){
    	Date d=new Date();
    	String filepath=d.toString().replace(" ","_").replace(":","_")+".png";
    	TakesScreenshot ts=(TakesScreenshot)driver;
    	File srcFile=ts.getScreenshotAs(OutputType.FILE);
    	String ReportsPath="C:\\report\\Screenshots\\"+filepath;
    	
    	try {
			FileUtils.copyFile(srcFile,new File(ReportsPath));
			test.log(LogStatus.INFO,test.addScreenCapture(ReportsPath));//to add screenshots to reports
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "PASS";
    	}
    
    public boolean isElementPresent(String password){
    	int s=driver.findElements(By.xpath(password)).size();
    	if(s==0)
    		return false;
    		else
    			return true;
    }
    public String reportFailure(String failureMessage){
		takeScreenShot();
		test.log(LogStatus.FAIL,failureMessage);
		Assert.fail(failureMessage);
		return "PASS";
		
		}
    }
    

