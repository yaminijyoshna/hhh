package BaseTest;


import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import POM.Utility.ExtentManager;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BaseTest {
	public WebDriver driver;
	public  ExtentReports rep=ExtentManager.getInstance();
	public ExtentTest test;
	
	public void init(String browserType){
		test.log(LogStatus.INFO, "opening the browser"+browserType);
		//if(!FBConstants.GRID)
		{		if(browserType.equals("Mozilla"))
		 driver=new FirefoxDriver();
		else if(browserType.equals("Chrome")){
			System.setProperty("webdriver.chrome.driver","C:\\Users\\Joe\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
			driver=new ChromeDriver();
		}
		/*}else{
		 //grid
		  
			DesiredCapabilities cap=null;
			if(browserType.equals("firefox")){
			    cap=DesiredCapabilities.firefox();
				cap.setBrowserName("firefox");
				cap.setJavascriptEnabled(true);
				cap.setPlatform(Platform.WIN10);
			}
				else if(browserType.equals("chrome")){
					cap=DesiredCapabilities.chrome();
					cap.setBrowserName("chrome");
					cap.setJavascriptEnabled(true);
					cap.setPlatform(Platform.WIN10);
					}
				try {
					driver=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), cap);
					//return Constants.PASS;
				} catch (MalformedURLException e) {
					
					e.printStackTrace();
				}
				}
			
			
		}*/
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		test.log(LogStatus.INFO, "closing the browser");
		}
	}
	public String reportFailure(String failureMessage){
		takeScreenShot();
		test.log(LogStatus.FAIL,failureMessage);
		Assert.fail(failureMessage);
		return "PASS";
		
		}
	public boolean isElementPresent(String LocatorKey){
	int s=	driver.findElements(By.xpath(LocatorKey)).size();
	if(s==0){
		test.log(LogStatus.INFO,"element not found"+LocatorKey);
		return false;
	}
	else{
		test.log(LogStatus.INFO,"element  found"+LocatorKey);
		return true;
	}
	}
	 public String takeScreenShot(){
	    	Date d=new Date();
	    	String filepath=d.toString().replace(" ","_").replace(":","_")+".png";
	    	TakesScreenshot ts=(TakesScreenshot)driver;
	    	File srcFile=ts.getScreenshotAs(OutputType.FILE);
	    	String ReportsPath="C:\\report\\Screenshots\\"+filepath;
	    	
	    	try {
				FileUtils.copyFile(srcFile,new File(ReportsPath));
				test.log(LogStatus.INFO,test.addScreenCapture(ReportsPath));//to add screenshots to reports
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "PASS";
	    	}
	    
}
