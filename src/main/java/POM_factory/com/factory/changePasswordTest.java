package POM_factory.com.factory;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import BaseTest.BaseTest;
import POM.Utility.DataUtil;
import POM.Utility.Xls_reader;

public class changePasswordTest extends BaseTest{
	String testCaseName="changePasswordTest";
	Xls_reader xls=new Xls_reader("C:\\Users\\Joe\\Desktop\\POMFF.xlsx");
	
	@Test(dataProvider="getData")
	public void changePasswordTest(Hashtable<String, String> data){
	test=rep.startTest("changePasswordTest");
	test.log(LogStatus.INFO, data.toString());
		if(!DataUtil.isRunnable(xls, testCaseName) || data.get("Runmode").equalsIgnoreCase("N")){
			test.log(LogStatus.SKIP,"Skipping the test if runmode is no" );
			throw new SkipException("Skipping the test if runmode is no");
          }
		test.log(LogStatus.INFO, "start the test");
		init(data.get("Browser"));
		LaunchPage launchP=new LaunchPage(driver, test);
		PageFactory.initElements(driver,launchP);
		LoginPage logP=launchP.goToLoginPage();
		 Object page=logP.doLogin(data.get("UserName"), data.get("CurrentPassword"));
		 if(page instanceof LoginPage){
			 reportFailure("Login failed");
		 takeScreenShot();
		 }
		LandingPage landP= (LandingPage)page;
		landP.menu.goToSettings();
		GeneralsettingsPage settings=new GeneralsettingsPage(driver, test);
		PageFactory.initElements(driver,settings);
		settings.goTOPasswordChange();
		String actualResult=settings.doPasswordChange(data.get("CurrentPassword"), data.get("Password"), data.get("ChangePassword"));
		System.out.println("result of changePasswordTest-"+actualResult);
		if(!actualResult.equals(data.get("ExpectedResult")))
			reportFailure("Failed to do changePasswordTest"+actualResult);
	    test.log(LogStatus.PASS, "Test Passed");
		}
	
	@DataProvider
	public Object[][] getData(){
		return DataUtil.getData(xls, testCaseName);
		
	}

	@AfterTest
public void quit(){
	if(rep!=null){
		rep.endTest(test);
		rep.flush(); 
	}
}
}
