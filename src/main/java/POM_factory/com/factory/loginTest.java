package POM_factory.com.factory;

import java.util.Hashtable;

import org.junit.Assert;
import org.junit.experimental.theories.DataPoint;
import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;

import BaseTest.BaseTest;
import POM.Utility.DataUtil;
import POM.Utility.ExtentManager;
import POM.Utility.Xls_reader;

public class loginTest extends BaseTest{
	String testCaseName="LoginTest";
	Xls_reader xls=new Xls_reader("C:\\Users\\Joe\\Desktop\\POMFF.xlsx");
	
	@Test(dataProvider="getData")
	public void doLogin(Hashtable<String, String> table){
		test=rep.startTest("LoginTest");
		test.log(LogStatus.INFO, table.toString());
		if( !DataUtil.isRunnable(xls, testCaseName) || table.get("Runmode").equals("N")){
			test.log(LogStatus.SKIP,"Skipping the test if runmode is no" );
			throw new SkipException("Skipping the test if runmode is no");
			}
		test.log(LogStatus.INFO,"openeing browser");
		init(table.get("Browser"));
		LaunchPage launchP=new LaunchPage(driver, test);
	     PageFactory.initElements(driver,launchP);
		LoginPage logP=launchP.goToLoginPage();
		test.log(LogStatus.INFO, "do Login"+table.get("UserName")+"__"+table.get("Password"));
		logP.takeScreenShot();
		Object page=logP.doLogin(table.get("UserName"), table.get("Password"));
		isElementPresent("//*[@id='pagelet_welcome_box']/ul/li/div/a");
		String actualResult="";
		if(page instanceof LoginPage){
			actualResult="Unsuccess";
		 logP.takeScreenShot();
		}
			else{
				actualResult="success";
				//LandingPage lp=(LandingPage)page.
			if(!actualResult.equals(table.get("ExpectedResult"))){
				reportFailure("Login failed");
			}
			}
			
		}
	
/*else if(page instanceof LandingPage){
			test.log(LogStatus.INFO,"login is successful");
			System.out.println("Login successful");
			LandingPage lp=(LandingPage)page;
			lp.gotoProfile();
			test.log(LogStatus.PASS,"Test PAsses");
			lp.takeScreenShot();
			//lp.VerifyTitle("", ExpectedText)
			}*/


	@DataProvider
	public Object[][] getData(){
		return DataUtil.getData(xls, testCaseName);
	}
	@AfterTest
	public void quit(){
		if(rep!=null){
			rep.endTest(test);
			rep.flush();
		}
		if(driver!=null){
			driver.quit();
		}
}
}
