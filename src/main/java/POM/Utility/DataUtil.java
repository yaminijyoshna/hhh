package POM.Utility;

import java.util.Hashtable;

import org.testng.annotations.DataProvider;



public class DataUtil {

	
	public static Object[][] getData(Xls_reader xls,String testName){
		String SheetName="Data";
		
		
		int testStartRowNum=1;
		while(!xls.getCellData(SheetName,0,testStartRowNum).equals(testName)){
			testStartRowNum++;
		}
		System.out.println("TestCase start fron row--"+testStartRowNum);
		
		int colStartRowNum=testStartRowNum+1;
		int dataStartRowNum=testStartRowNum+2;
		
		int rows=0;
		while(!xls.getCellData(SheetName,0,dataStartRowNum+rows).equals("")){
			rows++;
		}
		System.out.println("Total no of rows"+rows);
		
		int cols=0;
		while(!xls.getCellData(SheetName,cols,colStartRowNum).equals("")){
			cols++;
		}
		System.out.println("Total no of cols"+cols);
		
		Hashtable<String, String> table=null;
		Object [][] data=new Object[rows][1];
		
		//read data for a complete row
		int dataRow=0;
		for(int rNum=dataStartRowNum;rNum<dataStartRowNum+rows;rNum++){
			table=new Hashtable<String, String>();
			for(int cNum=0;cNum<cols;cNum++){
			 String key=xls.getCellData(SheetName, cNum, colStartRowNum);
			 String value=xls.getCellData(SheetName, cNum, rNum);
			 table.put(key, value);
				}
			data[dataRow][0]=table;
			dataRow++;
		}
		return data;
		
		
		}
	
	public static boolean isRunnable(Xls_reader xls,String testName){
	int rows=xls.getRowCount("TestCases");
	for(int rNum=2;rNum<=rows;rNum++){
	String TCID=xls.getCellData("TestCases","TCID", rNum);
	if(TCID.equals(testName)){
		String runmode=xls.getCellData("TestCases","Runmode", rNum);	
		if(runmode.equals("Y"))
			return true;//run the testcase don'i skip it
		else 
			return false;//skip the the test case
		}
	    }
	   return false;//default
	  }
      }
