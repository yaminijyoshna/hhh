package POM.Utility;

import java.io.File;
import java.util.Date;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;


public class ExtentManager {//reports.config.xml
 
private static ExtentReports rep;

public static ExtentReports getInstance(){
	if(rep==null){
		Date d=new Date();
		String filePath=d.toString().replace(":","_").replace(" ","_")+".html";
		//FBCstants.REPORTPATH
		rep=new ExtentReports("C:\\report\\"+filePath,true,DisplayOrder.NEWEST_FIRST);
		rep.loadConfig(new File(System.getProperty("user.dir")+"\\ReportsConfig.xml"));
	}
	return rep;
	
}
}
