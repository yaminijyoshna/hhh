package POM.Utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Xls_reader {
	String path="";
	 public FileInputStream fis=null;
	 public XSSFCell cell=null;
	 public XSSFWorkbook wb=null;
	 public XSSFRow row=null;
	 public XSSFSheet sheet=null;
	 
	 public Xls_reader(String path){
		 this.path=path;
		 try {
			fis=new FileInputStream(path);
			 wb=new XSSFWorkbook(fis);
			 sheet=wb.getSheetAt(0);
			 fis.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	 public int getRowCount(String SheetName){
		int index= wb.getSheetIndex(SheetName);
		if(index==-1)
			return 0;
		sheet=wb.getSheetAt(index);
		int number=sheet.getLastRowNum()+1;
		return number;
		 }
	 public int getColCount(String SheetName){
			int index= wb.getSheetIndex(SheetName);
			if(index==-1)
				return 0;
			sheet=wb.getSheetAt(index);
			row=sheet.getRow(0);
			return row.getLastCellNum();
	 }
	 public String getCellData(String SheetName,String colName,int rowNum){
		 try{
			int index= wb.getSheetIndex(SheetName);
			if(index==-1)
				return "";
			sheet=wb.getSheetAt(index);
			row=sheet.getRow(0);
			int colNum=-1;
			for(int i=0;i<row.getLastCellNum();i++){
				if(row.getCell(i).getStringCellValue().equalsIgnoreCase(colName))
					colNum=i;
			}
			if(rowNum<0)
				return "";
			row=sheet.getRow(rowNum-1);
			if(row==null)
				return "";
			if(colNum==-1)
				return "";
			cell=row.getCell(colNum);
			if(cell==null)
			return "";
			if(cell.getCellType()==cell.CELL_TYPE_STRING)
				return cell.getStringCellValue();
			else if(cell.getCellType()==cell.CELL_TYPE_BOOLEAN)
				return String.valueOf(cell.getBooleanCellValue());
			else if(cell.getCellType()==cell.CELL_TYPE_NUMERIC)
				return String.valueOf(cell.getNumericCellValue());
			else
				return "";
		 }catch(Exception e){
			 return rowNum +"and"+ colName+"was not found";
		 }
	 }
	 public String getCellData(String SheetName,int  colNum,int rowNum){
		 try{
			int index= wb.getSheetIndex(SheetName);
			if(index==-1)
				return "";
			sheet=wb.getSheetAt(index);
			
			if(rowNum<0)
				return "";
			row=sheet.getRow(rowNum-1);
			if(row==null)
				return "";
			if(colNum==-1)
				return "";
			cell=row.getCell(colNum);
			if(cell==null)
			return "";
			if(cell.getCellType()==cell.CELL_TYPE_STRING)
				return cell.getStringCellValue();
			else if(cell.getCellType()==cell.CELL_TYPE_BOOLEAN)
				return String.valueOf(cell.getBooleanCellValue());
			else if(cell.getCellType()==cell.CELL_TYPE_NUMERIC)
				return String.valueOf(cell.getNumericCellValue());
			else
				return "";
		 }catch(Exception e){
			 return rowNum +"and"+ colNum+"was not found";
		 }
	 }
 
}
